module.exports = {
  development: {
    api_host: 'http://localhost:3000',
  },

  production: {
    api_host: 'http://api.ericegan.io',
  },
};
