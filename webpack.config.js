var path = require('path');
var DotenvPlugin = require('dotenv-webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js",
    publicPath: '/',
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: path.resolve(__dirname, 'node_modules'),
        loader: 'babel-loader',
      },
      {
        test: /\.jsx?$/,
        exclude: path.resolve(__dirname, 'node_modules'),
        loader: 'eslint-loader',
        options: {
          fix: true,
        }
      },
      {
        exclude: path.resolve(__dirname, 'node_modules'),
          test: /\.s?css$/,
          loaders: [
            'style-loader?sourceMap',
            'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]',
            'sass-loader'
          ]
      },
      {
        exclude: path.resolve(__dirname, 'node_modules'),
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      },
    ],
  },

  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    port: 8080,
    historyApiFallback: true,
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: 'body',
    }),
    new DotenvPlugin(),
  ],
}
