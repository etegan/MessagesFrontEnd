import React from 'react';
import {BrowserRouter, Route } from 'react-router-dom';
import Home from './pages/home';
import styles from './styles.scss';
import CSSModules from 'react-css-modules';
import { connect } from 'react-redux';
import init from './actions.js';

function mapDispatchToProps(dispatch){
  return {
    init: (socket_id) => dispatch(init(socket_id)),
  };
}

function mapStateToProps(state){
  return {
    socket: state.socket,
  }; 
}

class App extends React.Component {
  render(){
    return(
      <BrowserRouter>
        <Route path='/' component={Home} />
      </BrowserRouter>

    );
  }
}

var app = CSSModules(App, styles);
export default connect(mapStateToProps, mapDispatchToProps)(app);
