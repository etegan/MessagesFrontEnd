import React from 'react';
import NewConversation from '../../components/new-conversation';
import UploadAvatar from '../../components/upload-avatar';
import Conversations from '../../components/conversations';
import { Route } from 'react-router-dom';
import Conversation from '../../components/conversation';
import Signing from '../../components/signing';
import CSSModules from 'react-css-modules';
import styles from './styles.scss';

class Home extends React.Component{
  render(){
    const { user } = this.props;
    return(
      <div styleName='home'>
        {user.isAuthenticated ?
          messageInterface()
          :
          signUp()
        }

        <Route path='/conversations/new' component={NewConversation} />
        <Route path='/upload-avatar' component={UploadAvatar} />
      </div>
    );
  }
}

function messageInterface(){
  return(
    <div styleName='conversation-panel'>
      <Conversations />
      <Route path='/conversations/:id' component={Conversation} />
    </div>
  );
}

function signUp(){
  return (
    <Signing />
  );
}

export default CSSModules(Home, styles);
