import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Home from './home.jsx';

function mapStateToProps(state){
  return {
    user: state.user,  
  }; 
}

export default withRouter(connect(mapStateToProps)(Home));
