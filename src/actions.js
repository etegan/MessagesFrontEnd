import {authenticate} from './services/api';

export default function init(socket_id){
  return function(dispatch){
    var token = localStorage.getItem('token') || null;
    if(token){
      dispatch(authenticating());
      authenticate(socket_id)
        .then(res => {
          return res.json();
        })
        .then(json => {
          dispatch(authenticationSucceeded(json));
        });
    }
  };
}

export const AUTHENTICATING = 'AUTHENTINCATING';
function authenticating(){
  return {
    type: AUTHENTICATING,
  };
}

export const AUTHENTICATION_SUCCEEDED = 'AUTHENTICATION_SUCCEEDED';
function authenticationSucceeded(user){
  return {
    type: AUTHENTICATION_SUCCEEDED,
    payload: user,
  };
}
