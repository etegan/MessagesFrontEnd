import fetch from 'isomorphic-fetch';
var config = require('../../../config')[process.env.NODE_ENV];

function tokenHeaders() {
  const token = localStorage.getItem('token') || null;
  return{
    'Authorization': `Bearer ${token}`,
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  };
}

const jsonHeaders = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
};

export function newConversation(user_id, recipient){
  var path = `${config.api_host}/users/${user_id}/conversations`; 
  var body = {
    recipient: recipient,
  };
  var options = {
    method: 'post',
    body: JSON.stringify(body),
    headers: tokenHeaders(),

  };
  return fetch(path, options);
}

export function getConversations(user_id){
  var path = `${config.api_host}/users/${user_id}/conversations`; 
  var options = {
    method: 'GET',
    headers: tokenHeaders(),
  };
  return fetch(path, options);
}

export function newMessage(message, conversation_id, user_id){
  var path = `${config.api_host}/conversations/${conversation_id}/messages`; 
  var body = {
    user_id: user_id,
    message: message,
    conversation_id: conversation_id,
  };
  var options = {
    method: 'post',
    body: JSON.stringify(body),
    headers: tokenHeaders(),

  };
  return fetch(path, options);
}


export function signIn(sign_in_data){
  var path = `${config.api_host}/login`; 
  var body = {
    email: sign_in_data.email,
    password: sign_in_data.password,
    socket: sign_in_data.socket,
  };
  var options = {
    method: 'post',
    body: JSON.stringify(body),
    headers: jsonHeaders,
  };
  return fetch(path, options);
}

export function authenticate(socket_id){
  var path = `${config.api_host}/authenticate`; 
  var body = {
    socket: socket_id,
  };
  var options = {
    method: 'post',
    body: JSON.stringify(body),
    headers: tokenHeaders(),
  };
  return fetch(path, options);
}

export function signUp(sign_up_info){
  return fetch(config.api_host+'/sign-up', 
    { 
      method: 'POST',
      headers: jsonHeaders,
      body: JSON.stringify(sign_up_info),
    }
  );
}

export function uploadAvatar(form, id){
  const token = localStorage.getItem('token') || null;
  return fetch(config.api_host+`/users/${id}/avatar`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    body: form,
  });

}
