import {add_socket_id} from './actions.js';
import init from '../../actions.js';
const config = require('../../../config')[process.env.NODE_ENV];
import {addMessage} from '../../components/conversation/actions.js';
import {conversationCreated} from '../../components/new-conversation/actions.js';
var io = require('socket.io-client');
var socket = null;

module.exports = function(store){
  socket = io.connect(config.api_host);

  socket.on('socket_id', (socket_id) => {
    store.dispatch(add_socket_id(socket_id));
    store.dispatch(init(socket_id));
  });

  socket.on('message', message => {
    store.dispatch(addMessage(message));
  });

  socket.on('conversation_created', conversation => {
    console.log('inside fe');
    store.dispatch(conversationCreated(conversation));
  });
}; 
