export const ADD_SOCKET_ID = 'ADD_SOCKET';
export function add_socket_id(socket_id){
  return {
    type: ADD_SOCKET_ID,
    payload: socket_id,
  };
}
