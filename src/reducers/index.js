import {combineReducers} from 'redux';
import conversations from './conversations.js';
import message_box from './conversation.js';
import user from './user.js';
import users from './users.js';
import sign_up from './sign_up.js';
import new_conversation from './new_conversation.js';
import socket from './socket.js';
import messages from './messages.js';
import sign_in from './sign_in.js';
import {SIGN_OUT} from '../components/header/actions.js';

const reducers = combineReducers({
  conversations,
  message_box,
  user,
  socket,
  sign_in,
  users,
  sign_up,
  new_conversation,
  messages,
});

export default function(state, action){
  if(action.type == SIGN_OUT){
    state = undefined;
  }
  return reducers(state, action);
}
