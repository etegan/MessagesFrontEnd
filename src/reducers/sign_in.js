import {CHANGE_SIGN_IN_EMAIL, CHANGE_SIGN_IN_PASSWORD} 
  from '../components/signing/components/sign-in/actions.js';

const initialState = {
  email: '',
  password: '',
};

export default function(state=initialState, action){
  const {type, payload} = action;

  switch(type){
  case CHANGE_SIGN_IN_EMAIL:{
    return{
      ...state,
      email: payload,
    };
  }
  case CHANGE_SIGN_IN_PASSWORD:{
    return {
      ...state,
      password: payload,
    };
  }
  }
  return state;
}
