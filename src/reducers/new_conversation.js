import {
  RECIPIENT_CHANGED,
} from '../components/new-conversation/actions.js';
const initialState = {
  recipient: '',
};

export default function(state=initialState, action){
  const {type, payload} = action;
  switch(type){
  case RECIPIENT_CHANGED: {
    return {
      ...state,
      recipient: payload,
    };
  }
  }
  return state;
}
