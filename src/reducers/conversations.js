import {ADD_CONVERSATIONS} from '../components/conversations/actions.js';
import {ADD_MESSAGE} from '../components/conversation/actions.js';
import {CONVERSATION_CREATED} from '../components/new-conversation/actions.js';

const initialState = {};

export default function(state = initialState, action){
  const {type, payload} = action;
  switch(type){
  case CONVERSATION_CREATED:
  case ADD_CONVERSATIONS:{
    console.log(payload);
    return {
      ...state,
      ...payload.conversations,
    };
  }
  case ADD_MESSAGE: {
    return addMessage(state, payload);
  } 
  }
  return state;
}

function addMessage(state, payload){
  const {conversation_id, id} = payload;
  var conversation = state[conversation_id];
  return {
    ...state,
    [conversation.id]: {
      ...conversation,
      messages: conversation.messages.concat(id), 
    }, 
  };
}
