import {
  SENDING_MESSAGE,
  CHANGE_MESSAGE, 
  OPEN_CONVERSATION} 
  from '../components/conversation/actions.js';

const initialState = {
  showButton: false,
  message: '',
};

export default function(state = initialState, action){
  const {type, payload} = action;
  switch(type){

  case SENDING_MESSAGE: {
    return {
      ...state,
      message: '',
    };
  }
  case CHANGE_MESSAGE: {
    return {
      ...state,
      message: payload,
    };
  }
  case OPEN_CONVERSATION: {
    return{
      ...state,
      ...payload,
    }; 
  }
  }
  return state;
}


