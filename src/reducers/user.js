import {SIGN_UP_SUCCEEDED} from '../components/signing/components/sign-up/actions.js';
import {SIGN_IN_SUCCEEDED} from '../components/signing/components/sign-in/actions.js';
import {AUTHENTICATION_SUCCEEDED} from '../actions.js';
import {ADD_AVATAR} from '../components/upload-avatar/actions.js';
const initialState = {
  isAuthenticated: false,
  username: '',
  email: '',
};

export default function(state = initialState, action){
  const {type, payload} = action;
  switch(type) {
  case AUTHENTICATION_SUCCEEDED:
  case SIGN_IN_SUCCEEDED:
  case SIGN_UP_SUCCEEDED: {
    return {
      ...state,
      username: payload.username,
      email: payload.email,
      id: payload.id,
      isAuthenticated: true,
    };
  }
  case ADD_AVATAR: {
    return {
      ...state,
      avatar: payload,
    };
  }
  }
  return state;
}
