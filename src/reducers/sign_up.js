import {
  EMAIL_CHANGE,
  USERNAME_CHANGE,
  PASSWORD_CHANGE,
  PASSWORD_CONFIRMATION_CHANGE} from '../components/signing/components/sign-up/actions.js';

const initalState = {
  username: '',
  email: '',
  password: '',
  passwordConfirmation: '',
};

export default function(state = initalState, action){
  const { type, payload } = action;

  switch(type){
  case EMAIL_CHANGE: {
    return {
      ...state,
      email: payload,
    };
  }
  case USERNAME_CHANGE: {
    return {
      ...state,
      username: payload,
    };
  }
  case PASSWORD_CHANGE: {
    return {
      ...state,
      password: payload,
    };
  }
  case PASSWORD_CONFIRMATION_CHANGE: {
    return {
      ...state,
      passwordConfirmation: payload,
    };
  }
  }
  return state;

}

