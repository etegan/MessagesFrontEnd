import {ADD_CONVERSATIONS} from '../components/conversations/actions.js';
import {SIGN_OUT} from '../components/header/actions.js';
import {CONVERSATION_CREATED} from '../components/new-conversation/actions.js';

const initialState = {};

export default function(state = initialState, action){
  const {type, payload} = action;
  switch(type){
      
  case CONVERSATION_CREATED:
  case ADD_CONVERSATIONS:{
    return {
      ...state,
      ...payload.users,
    };
  }
  case SIGN_OUT: {
    return {...initialState};
  }
  }
  return state;
}
