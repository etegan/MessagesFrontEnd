import {ADD_SOCKET_ID} from '../services/socket/actions.js';
const initialState = {
  id: '',
};
export default function(state = initialState, action){
  const {type, payload} = action;
  switch(type){
  case ADD_SOCKET_ID: {
    return {
      ...state,
      id: payload,
    };
  }
  }
  return state;
}
