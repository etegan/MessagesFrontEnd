import {ADD_CONVERSATIONS} from '../components/conversations/actions.js';
import {ADD_MESSAGE} from '../components/conversation/actions.js';
import {CONVERSATION_CREATED} from '../components/new-conversation/actions.js';


export default function(state = {}, action){
  switch(action.type){
  case CONVERSATION_CREATED:
  case ADD_CONVERSATIONS:{
    return {
      ...state,
      ...action.payload.messages,
    };
  }
  case ADD_MESSAGE: {
    return addMessage(state, action);
  }
  }
  return state;
}
function addMessage(state, action){
  const {payload} = action;
  const {id, message, user_id} = payload;

  const newMessage = {id: id, message: message, user_id: user_id};

  return {
    ...state,
    [id]: newMessage,
  };
}
