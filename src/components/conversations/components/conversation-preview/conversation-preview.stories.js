import React from 'react';
import Conversation from './';
import { storiesOf } from '@storybook/react';

storiesOf('Conversation List/Conversation', module)
  .add('Conversation', () => (<Conversation />)); 
