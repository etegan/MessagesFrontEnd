import React from 'react';
import {NavLink} from 'react-router-dom';
import CSSModules from 'react-css-modules';
import styles from './styles.scss';
import no_image from '../../../../../assets/no_image.gif';
import moment from 'moment';
const config = require('../../../../../config')[process.env.NODE_ENV];

class ConversationPreview extends React.Component {
  render(){
    const { last_message, conversation, recipient}  = this.props; 
    const timestamp = last_message ? moment(last_message.created_at) : null;
    return(
      <NavLink to={`/conversations/${conversation.id}`} styleName="conversation-preview">
        <img styleName="img-preview" src={recipient.avatar ? config.api_host + recipient.avatar : no_image}/>
        <div styleName="info">
          <div styleName="header">
            <span styleName="recipient">{recipient.username}</span>
            <span styleName="timestamp">{timestamp && timestamp.format('HH:mm')}</span>
          </div>  
          <span styleName="text-preview">{last_message && last_message.message}</span>
        </div>
      </NavLink>
    );
  }
}

export default CSSModules(ConversationPreview, styles);
