

export const OPEN_CONVERSATION = 'OPEN_CONVERSATION';
export function openConversation(conversation){
  return {
    type: OPEN_CONVERSATION,
    payload: conversation,
  };
}
