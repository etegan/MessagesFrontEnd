import ConversationPreview from './conversation-preview.jsx';
import {connect} from 'react-redux';
import {openConversation} from './actions';
import {withRouter} from 'react-router-dom';
function mapDispatchToProps(dispatch, ownProps){
  return {
    openConversation: () => dispatch(openConversation(ownProps.conversation)),
  };
}

function mapStateToProps(state, ownProps){
  const {conversation} = ownProps;
  return{
    recipient: get_recipients(conversation.users, state.user.id, state.users),
    last_message: get_last_message(conversation.messages, state.messages),
  };
}

function get_recipients(participants, user_id, users){
  var id = participants.filter(participant => participant != user_id);
  if(!users[id[0]]){
    console.log(id);
    console.log(participants);
    console.log(user_id);
    console.log(users);
  }
  return users[id[0]]; 
}

function get_last_message(message_ids, messages){
  var message_id = message_ids[message_ids.length - 1];
  return message_id ? messages[message_id] : null;
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConversationPreview));
