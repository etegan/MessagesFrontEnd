import { connect } from 'react-redux';
import Conversations from './conversations.jsx';
import {loadConversations} from './actions';
import {withRouter} from 'react-router-dom';

function mapDispatchToProps(dispatch){
  return {
    loadConversations: (user_id) => dispatch(loadConversations(user_id)),
  };
}

function mapStateToProps(state){
  return{
    conversations: state.conversations,
    user: state.user,
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Conversations));
