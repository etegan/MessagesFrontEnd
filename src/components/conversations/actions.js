import {getConversations} from '../../services/api';
import {normalizeConversations} from './normalizer.js';

export function loadConversations(user_id){
  return function(dispatch){
    getConversations(user_id)
      .then(res => {
        return res.json();
      })
      .then(json => {
        dispatch(addConversations(json));
      });
  };
}

export const ADD_CONVERSATIONS  = 'ADD_CONVERSATIONS';
export function addConversations(conversations){ 
  return {
    type: ADD_CONVERSATIONS,
    payload: normalizeConversations({conversations: conversations}).entities,

  };
}
