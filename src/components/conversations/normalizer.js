import {normalize, schema} from 'normalizr';

export function normalizeConversations(conversations){
  
  const user = new schema.Entity('users');
  const message = new schema.Entity('messages');
  const conversation = new schema.Entity('conversations', {
    messages: [message],
    users: [user],
  });

  const mySchema = {conversations: [conversation]};
  return normalize(conversations, mySchema);
}
