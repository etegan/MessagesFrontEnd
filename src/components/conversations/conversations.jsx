import React from 'react';
import ConversationPreview from './components/conversation-preview';
import styles from './styles.scss';
import CSSModules from 'react-css-modules';
import {Link} from 'react-router-dom';


class Conversations extends React.Component {
  render(){
    const { conversations } = this.props;
    return(
      <div styleName="conversations">
        <div styleName='conversations-header'>
          <i className='fa fa-user-circle'></i><i className='fa fa-sign-out'></i>
        </div>
        {Object.keys(conversations).length > 0 ? 
          nonEmptyList(conversations)
          :
          emptyList()
        }
        <div styleName='conversations-footer'>
          <Link to='/conversations/new'><i className='fa fa-plus-circle'></i></Link>
        </div>
      </div>
    );

  }

  componentDidMount(){
    const {user, loadConversations} = this.props;
    loadConversations(user.id);
  }
}

function nonEmptyList(conversations){
  return (
    <ul styleName='conversation-list'>
      {Object.values(conversations).map(
        (conversation, i) => (
          <li key={i}><ConversationPreview conversation={conversation} /></li>
        )
      )}
    </ul>
  );
}

function emptyList(){
  return(
    <div styleName="list-message">
      <span>There are no conversations!</span>
    </div>
  ); 
}

export default CSSModules(Conversations, styles);
