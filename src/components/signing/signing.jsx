import React from 'react';
import SignUp from './components/sign-up';
import SignIn from './components/sign-in';
import {Link, Route} from 'react-router-dom';

class Signing extends React.Component{
  
  render(){
    return (
      <div>
        <Link to='/sign-in'>Sign In</Link>
        <Link to='/sign-up'>Sign Up</Link>
        <div>
          <Route path='/sign-in' component={SignIn} />
          <Route path='/sign-up' component={SignUp} />
        </div>
      </div>
    );
  }
}

export default Signing;
