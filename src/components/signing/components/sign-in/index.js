import SignIn from './sign_in.jsx';
import {signingIn, changeSignInEmail, changeSignInPassword} from './actions.js';
import {connect} from 'react-redux';

function mapDispatchToProps(dispatch){
  return {
    changeSignInEmail: (event) => dispatch(changeSignInEmail(event)),
    changeSignInPassword: (event) => dispatch(changeSignInPassword(event)),
    signingIn: (formInfo) => dispatch(signingIn(formInfo)),
  };
}

function mapStateToProps(state){
  return{
    user: state.user,
    sign_in: state.sign_in,
    socket: state.socket,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
