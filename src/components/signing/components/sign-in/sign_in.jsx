import React from 'react';

class SignIn extends React.Component{
      
  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event){
    const { sign_in, signingIn, socket } = this.props;
    event.preventDefault();
    var sign_in_data = {
      email: sign_in.email,
      password: sign_in.password,
      socket: socket.id,
    };
    signingIn(sign_in_data);
  }
  render(){
    const { changeSignInEmail, changeSignInPassword } = this.props;
    return(
      <form onSubmit={this.handleSubmit}>
        <span>Email:</span>
        <input type='email' onChange={changeSignInEmail}/>
        <span>Password:</span>
        <input type='password' onChange={changeSignInPassword}/>
        <input type='submit' value='Sign in'/>
      </form>
    );
  }
}

export default SignIn;
