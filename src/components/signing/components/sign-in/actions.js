import {signIn} from '../../../../services/api';
export const SIGNIN_IN  = 'SIGNING_IN';
export function signingIn(formInfo){
  return function(dispatch){
    signIn(formInfo)
      .then(res => {
        return res.json();
      })
      .then(user => {
        dispatch(signInSucceeded(user));
      });
  };
}


export const SIGN_IN_SUCCEEDED = 'SIGN_IN_SUCCEEDED';
export function signInSucceeded(json){
  localStorage.setItem('token', json.token);
  return {
    type: SIGN_IN_SUCCEEDED,
    payload: json,
  };
}


export const CHANGE_SIGN_IN_EMAIL = 'CHANGE_SIGN_IN_EMAIL';
export function changeSignInEmail(event){
  return {
    type: CHANGE_SIGN_IN_EMAIL,
    payload: event.target.value,
  };
}
  
export const CHANGE_SIGN_IN_PASSWORD = 'CHANGE_SIGN_IN_PASSWORD';
export function changeSignInPassword(event){
  return {
    type: CHANGE_SIGN_IN_PASSWORD,
    payload: event.target.value,
  };
}

