import SignUp from './sign-up.jsx';
import { connect } from 'react-redux';
import { 
  signingUp,
  usernameChange,
  passwordChange,
  emailChange,
  passwordConfirmationChange} from './actions';

function mapDispatchToProps(dispatch){
  return {
    signingUp: (sign_up_info) => dispatch(signingUp(sign_up_info)),
    emailChange: (event) => dispatch(emailChange(event)),
    usernameChange: (event) => dispatch(usernameChange(event)),
    passwordChange: (event) => dispatch(passwordChange(event)),
    passwordConfirmationChange: (event) => dispatch(passwordConfirmationChange(event)),
  };
}

function mapStateToProps(state){
  return {
    sign_up: state.sign_up,
    socket: state.socket,
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
