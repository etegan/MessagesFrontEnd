import {signUp} from '../../../../services/api';
import Promise from 'bluebird';

function parseJSON(response){
  return new Promise((resolve, reject) => {
    response.json().then(json => {
      if(response.ok){
        resolve(json);
      }else{
        reject(new Error(json.error));
      }
    });
  }); 
}

export const SIGNING_UP  = 'SIGNING_UP';
export function signingUp(sign_up_info){
  return function(dispatch){
    dispatch(startSignUp());
    signUp(sign_up_info)
      .then(function(res) {
        return parseJSON(res);
      })
      .then(json => {
        dispatch(signUpSucceeded(json));
      })
      .catch(errIgnored => {
      });
  };
}

export const START_SIGN_UP = 'START_SIGN_UP';
export function startSignUp(){
  return {
    type: START_SIGN_UP,
  };
}

export const SIGN_UP_SUCCEEDED = 'SIGN_UP_SUCCEDDED';
export function signUpSucceeded(json){
  localStorage.setItem('token', json.user.token);
  return {
    type: SIGN_UP_SUCCEEDED,
    payload: json.user,
  };
}

export const SIGN_UP_FAILED = 'SIGN_UP_FAILED';
export function signUpFailed(){
  return {
    type: SIGN_UP_FAILED,
  };
}

export const EMAIL_CHANGE = 'EMAIL_CHANGE';
export function emailChange(event){
  return {
    type: EMAIL_CHANGE,
    payload: event.target.value,
  };
}


export const USERNAME_CHANGE = 'USERNAME_CHANGE';
export function usernameChange(event){
  return {
    type: USERNAME_CHANGE,
    payload: event.target.value,
  };
}


export const PASSWORD_CHANGE = 'PASSWORD_CHANGE';
export function passwordChange(event){
  return {
    type: PASSWORD_CHANGE,
    payload: event.target.value,
  };
}

export const PASSWORD_CONFIRMATION_CHANGE = 'PASSWORD_CONFIRMATION_CHANGE';
export function passwordConfirmationChange(event){
  return {
    type: PASSWORD_CONFIRMATION_CHANGE,
    payload: event.target.value,
  };
}
