import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './styles.scss';

class SignUp extends React.Component{
  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event){
    const { sign_up, signingUp, socket } = this.props;
    event.preventDefault();
    var sign_up_info = {
      username: sign_up.username,
      email: sign_up.email,
      password: sign_up.password,
      passwordConfirmation: sign_up.passwordConfirmation,
      socket: socket.id,
    };
    signingUp(sign_up_info);
  }
  render(){
    const { 
      emailChange,
      passwordChange,
      usernameChange,
      passwordConfirmationChange} = this.props;
    return(
      <form onSubmit={this.handleSubmit} styleName='sign-up'>
        <span>Username: </span>
        <input type='text' onChange={usernameChange}/>
        <span>Email:</span>
        <input type='email' onChange={emailChange}/>
        <span>Password:</span>
        <input type='password' onChange={passwordChange}/>
        <span>Confirm password: </span>
        <input type='password' onChange={passwordConfirmationChange}/>
        <input type='submit' value='Sign up'/>
      </form>
    );
  }
}

export default CSSModules(SignUp, styles);
