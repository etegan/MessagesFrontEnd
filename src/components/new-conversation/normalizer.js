import {normalize, schema} from 'normalizr';

export function normalizeConversation(data){
  
  const user = new schema.Entity('users');
  const message = new schema.Entity('messages');
  const conversation = new schema.Entity('conversations', {
    messages: [message],
    users: [user],
  });

  return normalize(data, conversation);
}
