import {newConversation} from '../../services/api';
import {normalizeConversation} from './normalizer.js';
export const RECIPIENT_CHANGED = 'RECIPIENT_CHANGED';
export function recipientChanged(event) {
  return {
    type: RECIPIENT_CHANGED,
    payload: event.target.value,
  };
}

export function recipientSubmitted(user_id, recipient){
  return function(dispatch){
    newConversation(user_id, recipient)
      .then(res =>{
        return res.json();
      })
      .then(conversation =>{
        dispatch(conversationCreated(conversation));
      });
  };
}

export const CONVERSATION_CREATED = 'CONVERSATION_CREATED';
export function conversationCreated(conversation){
  return {
    type: CONVERSATION_CREATED,
    payload: normalizeConversation(conversation).entities,
  };
}

