import NewConversation from './new-conversation.jsx';
import {recipientChanged, recipientSubmitted} from './actions';
import {connect} from 'react-redux';

function mapDispatchToProps(dispatch){
  return{
    recipientChanged: (event) => dispatch(recipientChanged(event)),
    recipientSubmitted: (user_id, recipient) => dispatch(recipientSubmitted(user_id, recipient)),
  };
}

function mapStateToProps(state){
  return{
    new_conversation: state.new_conversation,
    user: state.user,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewConversation);
