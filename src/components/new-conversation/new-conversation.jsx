import React from 'react';
import styles from './styles.scss';
import CSSModules from 'react-css-modules';

class NewConversation extends React.Component{
  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event){
    event.preventDefault();
    const {user, recipientSubmitted, new_conversation } = this.props;
    recipientSubmitted(user.id, new_conversation.recipient);
  }

  render(){
    const {new_conversation, recipientChanged} = this.props; 
    return (
      <div styleName='new-conversation'>
        <div styleName='new-conversation-box'>
          <form onSubmit={this.handleSubmit} >
            <input value={new_conversation.recipient} onChange={recipientChanged} type='text'/>
            <input type='submit'/>
          </form>
        </div>
      </div>
    );
  }
}

export default CSSModules(NewConversation, styles);
