import React from 'react';
import styles from './styles.scss';
import CSSModules from 'react-css-modules';


class UploadAvatar extends React.Component{

  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event){
    event.preventDefault();
    const { user , submitAvatar} = this.props;
    var form = new FormData(event.target);
    submitAvatar(form, user.id);

  }

  render(){
    return(
      <div styleName='upload-avatar'>
        <form onSubmit={this.handleSubmit} encType='multipart/form-data'>
          <input type='file' name='avatar' />
          <input type='submit'/>
        </form>
      </div>
    );
  }
}

export default CSSModules(UploadAvatar, styles);
