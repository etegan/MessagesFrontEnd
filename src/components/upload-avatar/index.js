import UploadAvatar from './upload_avatar.jsx';
import {connect} from 'react-redux';
import {submitAvatar} from './actions.js';

function mapDispatchToProps(dispatch){
  return {
    submitAvatar: (form, id) => dispatch(submitAvatar(form, id)),
  };
}

function mapStateToProps(state){
  return{
    user: state.user,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadAvatar);
