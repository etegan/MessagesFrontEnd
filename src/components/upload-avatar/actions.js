import {uploadAvatar} from '../../services/api';

export function submitAvatar(form, id){
  return function(dispatch){
    uploadAvatar(form, id)
      .then(res => {
        return res.json();
      })
      .then(json => {
        dispatch(addAvatar(json.avatar));
      });
  
  };
}


export const ADD_AVATAR = 'ADD_AVATAR';
export function addAvatar(avatar){
  return {
    type: ADD_AVATAR,
    payload: avatar,
  };
}
