import { storiesOf } from '@storybook/react';
import React from 'react';
import Chat from './chat.jsx';


const messages = [
  {
    status: "receiving",
    message: "here is a test message to se how it looks with som more text. still typing to make this message a litte longer. Thank you for your time. Good day",
  },
  {
    status: "sending",
    message: "here is a test message to se how it looks with som more text. still typing to make this message a litte longer. Thank you for your time. Good day",
  },
]

storiesOf('Chat/Chat', module)
  .add('Chat', () => (<Chat messages={messages} />));
