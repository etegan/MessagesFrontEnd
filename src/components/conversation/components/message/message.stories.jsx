import { storiesOf } from '@storybook/react';
import Message from './message.jsx';
import React from 'react';

const message = {
  status: "sending",
  message: "here is a test message to se how it looks with som more text. still typing to make this message a litte longer. Thank you for your time. Good day",
}
const message2 = {
  status: "receiving",
  message: "here is a test message to se how it looks with som more text. still typing to make this message a litte longer. Thank you for your time. Good day",
}

storiesOf('Chat/Message', module)
  .add('Message', () => (
    <div>
      <Message message={message} />
      <Message message={message2} />
    </div>
  ));
