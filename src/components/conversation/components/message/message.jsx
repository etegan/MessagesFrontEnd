import React from 'react';
import styles from './styles.scss';
import CSSModules from 'react-css-modules';

class Message extends React.Component {
  render(){
    const { message_id, messages, user } = this.props;
    return (
      <div styleName='message-wrapper'>
        <div styleName={messages[message_id].user_id == user.id ? 'sending' : 'receiving'}>
          {renderMessage(messages[message_id].message)}
        </div>
      </div>
    );
  }
}

function renderMessage(message){
  return message.split('\n').map((item, i) => {
    return(
      <span key={i}>
        {item}
        <br />
      </span>
    );
  });
}

export default CSSModules(Message, styles);
