import {connect} from 'react-redux';
import Message from './message.jsx';

function mapStateToProps(state){
  return {
    user: state.user,
    messages: state.messages,
  };
}

export default connect(mapStateToProps)(Message);
