import React from 'react';
import Message from './components/message';
import CSSModules from 'react-css-modules';
import styles from './styles.scss';


class Conversation extends React.Component {

  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  handleSubmit(event){
    const {user, sendMessage, message_box, match} = this.props;
    event.preventDefault();
    sendMessage(message_box.message, match.params.id, user.id);
  }

  handleKeyDown(event){
    if(event.charCode == 13 && event.shiftKey == false) {
      const {user, sendMessage, message_box, match} = this.props;
      event.preventDefault();
      sendMessage(message_box.message, match.params.id, user.id);
    }
  }

  render(){
    const { changeMessage, message_box, conversation } = this.props;
    return(
      <div styleName='conversation'>
        <div styleName='conversation-header'>
        </div>
        <div ref ={(conversationArea => this.conversationArea = conversationArea)} styleName='conversation-area'>
          {conversation && conversation.messages.map(
            (message, i) => (
              <Message key={i} message_id={message} />
            )
          )}
        </div>
        <div styleName='form-area'>
          <form onSubmit={this.handleSubmit}>
            <textarea value={message_box.message} onChange={changeMessage} styleName='conversation-input' onKeyPress={this.handleKeyDown}></textarea>
            <input type='submit'/>
          </form>
        </div>
      </div>
    );
  }

  componentDidUpdate(){
    this.conversationArea.scrollTop = this.conversationArea.scrollHeight;
  }
}


export default CSSModules(Conversation, styles);
