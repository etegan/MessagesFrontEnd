import Conversation from './conversation.jsx';
import { sendMessage, changeMessage, openConversation } from './actions.js';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

function mapDispatchToProps(dispatch){
  return {
    sendMessage: (message, conversation_id, user_id) => dispatch(sendMessage(message, conversation_id, user_id)),
    changeMessage: (event) => dispatch(changeMessage(event)),
    openConversation: (conversation_id) => dispatch(openConversation(conversation_id)),
  };
}

function mapStateToProps(state, ownProps){
  return {
    user: state.user,
    conversation: state.conversations[ownProps.match.params.id],
    message_box: state.message_box,
  };
}
var conversation = connect(mapStateToProps, mapDispatchToProps)(Conversation);
export default withRouter(conversation);
