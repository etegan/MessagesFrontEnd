import {normalize, schema} from 'normalizr';

export function normalizeConversations(conversations){
  const conversation = new schema.Entity('conversations');
  const schema = {conversations: [conversation]};
  return normalize(conversations, schema);
}
