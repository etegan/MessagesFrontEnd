import { newMessage } from '../../services/api';

export function sendMessage(message, conversation_id, user_id){
  return function(dispatch){
    dispatch(sendingMessage());
    newMessage(message, conversation_id, user_id).then(res => {
      return res.json();
    })
      .then(json => {
        dispatch(addMessage(json.message));
      });
  };
}


export const SENDING_MESSAGE = 'SENDING_MESSAGE';
export function sendingMessage(){
  return {
    type: SENDING_MESSAGE,
  };
}

export const OPEN_CONVERSATION = 'OPEN_CONVERSATION';
export function openConversation(conversation){
  return {
    type: OPEN_CONVERSATION,
    payload: conversation,
  };
}

export const ADD_MESSAGE = 'ADD_MESSAGE';
export function addMessage(message){
  return {
    type: ADD_MESSAGE,
    payload: message,
  };
}


export const CHANGE_MESSAGE = 'CHANGE_MESSAGE';
export function changeMessage(event){
  return {
    type: CHANGE_MESSAGE,
    payload: event.target.value,
  };
}
