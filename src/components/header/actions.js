
export const SIGN_OUT = 'SIGN_OUT';
export function signOut(eventIgnored){
  localStorage.removeItem('token');
  return {
    type: SIGN_OUT,
  };
}
