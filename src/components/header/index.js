import { connect } from 'react-redux';
import Header from './header.jsx';
import {signOut} from './actions.js';

function mapDispatchToProps(dispatch){
  return {
    signOut: (event) => dispatch(signOut(event)),
  };
}

function mapStateToProps(state){
  return {
    user: state.user,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
