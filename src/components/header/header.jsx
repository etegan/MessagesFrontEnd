import React from 'react';
import styles from './header.scss';
import {Link} from 'react-router-dom';
import CSSModules from 'react-css-modules';

class Header extends React.Component{

  render(){
    const {user, signOut} = this.props;
    return(
      <div styleName="header">
        <h1>Messages</h1>
        {user.signedIn ? 
          <Link to='/sign-in'>Sign In</Link>
          :
          <nav>
            <Link to='/' onClick={signOut}>Sign Out</Link>
            <Link to='/conversations/new'>New Conversation</Link>
            <Link to='/upload-avatar'>Upload Avatar</Link> 
          </nav>
        }
      </div>
    );
  }
}

export default CSSModules(Header, styles);
