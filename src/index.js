import React from 'react';
import ReactDOM from 'react-dom';
import App from './app.jsx';
import { Provider } from 'react-redux';
import reducers from './reducers';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';



const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(
    thunk
  )    
);
require('./services/socket')(store);

ReactDOM.render(
  <Provider store={store}>
    <App store={store} /></Provider>, document.getElementById('app'));

