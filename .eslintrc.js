module.exports = {
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": 'module',
        "ecmaFeatures":{
            "jsx": true,
            "experimentalObjectRestSpread": true,
        },
    },
    "extends": ["eslint:recommended"],
    
    "rules":{
        "semi": 2,
        "indent": ["error", 2],
        "comma-dangle": [2, 'always-multiline'],
        "no-console": 'warn',
        "no-unused-vars": ["error", {"argsIgnorePattern": "[iI]gnore"}], 
        "react/jsx-uses-react": 'error',
        "react/jsx-uses-vars": 'error',
        
    },
    "env":{
        browser: true,
        node: true,
    },
    "plugins":[
        'react'
    ]
}
