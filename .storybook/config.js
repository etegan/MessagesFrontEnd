import { configure, addDecorator } from '@storybook/react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from '../src/reducers.js';
import React from 'react';
import thunk from 'redux-thunk';

const req = require.context('../src', true, /\.stories.js/);
const store = createStore(
  reducers,
  applyMiddleware(
    thunk
  )
);

function loadStories() {
  req.keys().forEach( (key) => req(key));
}

addDecorator((story) => (
  <Provider store={store}>
    {story()}
  </Provider>
))

configure(loadStories, module);
